users = [];

let tablesection = document.querySelector("#tablesection");
let table = document.querySelector("#table");
let tablebody = document.querySelector("#tablebody")

let adduserbuttonsection = document.querySelector("#addbuttonsec");
let adduser = document.querySelector("#adduser")

let userformsec = document.querySelector("#formsection");

let userform = document.querySelector("form");

let formvisible=false;

function buildTable(){
    // Remove previous list
    tablebody.innerHTML='';
    if (users && users.length>0){

        // Rebuild list
        for(let user of users){
            let userrow = document.createElement("tr");
            let num = document.createElement("td")
            let name = document.createElement("td")
            let email = document.createElement("td")

            num.textContent = user.id;
            name.textContent = user.name;
            email.textContent = user.email;

            userrow.appendChild(num);
            userrow.appendChild(name);
            userrow.appendChild(email);

            tablebody.appendChild(userrow);

            num.onclick=()=>{alert(`Clicked User Number ${user.id}`)};
            name.onclick=()=>{alert(`Clicked User Name ${user.name}`)};
            email.onclick=()=>{alert(`Clicked User Email ${user.email}`)};

        }
    }else{
        let nodatarow = document.createElement("tr");
        let nodatacell = document.createElement("td")
        nodatacell.colSpan=3;
        nodatacell.textContent="No Data";
        
        nodatarow.appendChild(nodatacell);
        tablebody.appendChild(nodatarow);
    }

}

function showUserForm(){
    adduserbuttonsection.style.display="none";
    userformsec.style.display="block"
}

function addUser(user){
    users.push(user);
    buildTable();
    userformsec.style.display="none";
    adduserbuttonsection.style.display="block";
}

adduser.onclick = ()=>showUserForm();
userform.onsubmit= (ev)=>{ev.preventDefault();
    addUser(
        {
            id:users.length+1,
            name:userform.name.value,
            email:userform.email.value
        }
    )
}

buildTable();